<h1>VCard Builder</h1>

<div id="vcadr_exemple" class="vcard">
	<div class="vcard_head">
		<span class="vcard_name">
			Michel dupond
		</span>
		<span class="vcard_role">
			Expert comptable
		</span>
	</div>
	<div class="vcard_content">
		<div class="vcard_picture">
			<?php echo Asset::img('profil-avatar-type.jpg', array('id' => 'profil-avatar-type')); ?>
		</div>
		<div class="vcard_basics_infos">
			<ul>
				<li>28 ans</li>
				<li>0684567335</li>
				<li>contact@micheldupont.com</li>
			</ul>
		</div>
		<div class="vcard_decription">
			Diplomé depuis 2009 (DEC é DCG), rigoureux et dynamique
		</div>
	</div>
	<div class="vcard_footer">
		<div class="vcard_social">
			<a class="si si_facebook"></a>
			<a class="si si_twitter"></a>
		</div>
		<span class="vcard_action">
			<a href="" >Contact</a>
			<a href="" >Telecharger</a>
		</span>
	</div>
</div>