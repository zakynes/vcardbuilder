<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $title ?></title>

		<?php echo Asset::css('http://fonts.googleapis.com/css?family=Audiowide'); ?>
		<?php echo Asset::css('http://fonts.googleapis.com/css?family=Lobster'); ?>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<?php echo Asset::css('main.css'); ?>
		<?php echo Asset::css('socialicons.css'); ?>
		<?php
		foreach($cssFiles as $value)
		{
			echo Asset::css($value . '.css');
		}
		foreach($jsFiles as $value)
		{
			echo Asset::js($value . '.css');
		}
		foreach($css as $value)
		{
			echo Asset::css($value, array(), 'inline', true);
		}
		foreach($js as $value)
		{
			echo Asset::css($value, array(), 'inline', true);
		}
		?>
	</head>
	<body>
		<header>
			<div id="auth">
				<?php
						if($authCheck):
				?>
				<span>
				<a href="<?php echo Uri::create('/'); ?>" class=""><?php echo $authUsername; ?></a>
				</span>
				<a href="<?php echo Uri::create('/'.__('routes.auth.logout')); ?>" class="button">Logout</a>
				<?php
					else:
				?>
				<form action="<?php echo Uri::create('/'.__('routes.auth.login')); ?>" method="post">
					<input type="text" name="username" placeholder="Username" />
					<input type="password" name="password" placeholder="Password" />
					<input type="checkbox" name="remember" title="Connexion auto"/>
					<input type="submit" value="Login" />
				</form>
				<a href="<?php echo Uri::create('/'.__('routes.auth.register')); ?>" class="button">Register</a>
				<?php
					endif;
				?>
			</div>
			<div id="banner">
				VCard Builder
			</div>
			<nav id="menu">
				<ul>
					<li>
						<a href="<?php echo Uri::create('/'); ?>">Accueil</a>
					</li>
				</ul>
			</nav>
		</header>

		<div id="content">
			<?php echo (isset($message))?$message:''; ?>
			<?php echo $content ?>
		</div>

		<footer>
		</footer>
	</body>
</html>
