<?php

return array(
	'routes' => array(
		'auth' => array(
			'login' => 'Connexion',
			'logout' => 'Deconnexion',
			'register' => 'Inscription',
			'lospassword' => 'Recuperation-mot-de-passe'
		),
		'vcard' => array(
			'new' => 'Nouveau',
			'edit' => 'Edition',
			'infos' => 'Infos',
			'save' => 'Sauvegarder'
		)
	)
		);

