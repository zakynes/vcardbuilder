<?php
/**
 * Description of accueil
 *
 * @author Lenak
 */

class Controller_Accueil extends Controller_Main
{
	public function action_index()
	{
        $this->template->title = 'Accueil';
		$this->addCss('accueil');
        $this->render('accueil/index');
	}
}
