<?php

/**
 * Description of user
 *
 * @author Lenak
 */
class Controller_User extends Controller_Main
{

	public function action_login()
	{
		// already logged in?
		if(\Auth::check())
			$this->template->message = 'Déjà connecté';

		// was the login form posted?
		else if(Input::method() == 'POST')
		{
			// check the credentials.
			if(Auth::instance()->login(Input::param('username'),
							Input::param('password')))
			{
				if(Input::param('remember', false))
					Auth::remember_me();
				else
					Auth::dont_remember_me();
			}
			else
				$this->template->message = 'Echec de connexion';
		}
		
		Response::redirect_back('');
	}

	public function action_logout()
	{
		Auth::dont_remember_me();
		Auth::logout();
		Response::redirect_back();
	}

	public function action_register()
	{
		if(!Config::get('application.user.registration', false))
		{
			$this->template->message = 'Inscriptions désactivé';
			Response::redirect_back();
		}

		if(Input::method() == 'POST')
		{
			$created = Auth::create_user(
							Input::post('username'), Input::post('password'),
							Input::post('email'),
							Config::get('application.user.default_group', 1)
			);
			
			if($created)
			{
				$this->template->message = 'created';
				Response::redirect_back('');
			}
			else
				$this->template->message = 'fail';
		}
		
        $this->template->title = 'Register';
        $this->render('user/register');
	}

	public function action_lostpassword($hash = null)
	{
		// was the lostpassword form posted?
		if(Input::method() == 'POST')
		{
			// do we have a posted email address?
			if($email = Input::post('email'))
			{
				// do we know this user?
				if($user = \Model\Auth_User::find_by_email($email))
				{
					// generate a recovery hash
					$hash = \Auth::instance()->hash_password(\Str::random()) . $user->id;

					// and store it in the user profile
					\Auth::update_user(
							array(
						'lostpassword_hash' => $hash,
						'lostpassword_created' => time()
							), $user->username
					);

					// send an email out with a reset link
					\Package::load('email');
					$email = \Email::forge();

					// use a view file to generate the email message
					$email->html_body(
							\Theme::instance()->view('login/lostpassword')
									->set('url', \Uri::create('login/lostpassword/' . $hash), false)
									->set('user', $user, false)
									->render()
					);

					// give it a subject
					$email->subject(__('login.password-recovery'));

					// add from- and to address
					$from = \Config::get('application.email-addresses.from.website',
									'website@example.org');
					$email->from($from['email'], $from['name']);
					$email->to($user->email, $user->fullname);

					// and off it goes (if all goes well)!
					try
					{
						// send the email
						$email->send();
					}

					// this should never happen, a users email was validated, right?
					catch(\EmailValidationFailedException $e)
					{
						// \Messages::error(__('login.invalid-email-address'));
						\Response::redirect_back();
					}

					// what went wrong now?
					catch(\Exception $e)
					{
						// log the error so an administrator can have a look
						logger(\Fuel::L_ERROR,
								'*** Error sending email (' . __FILE__ . '#' . __LINE__ . '): ' . $e->getMessage());

						// \Messages::error(__('login.error-sending-email'));
						\Response::redirect_back();
					}
				}
			}

			// posted form, but email address posted?
			else
			{
				// inform the user and fall through to the form
				// \Messages::error(__('login.error-missing-email'));
			}

			// inform the user an email is on the way (or not ;-))
			// \Messages::info(__('login.recovery-email-send'));
			\Response::redirect_back();
		}

		// no form posted, do we have a hash passed in the URL?
		elseif($hash !== null)
		{
			// get the userid from the hash
			$user = substr($hash, 44);

			// and find the user with this id
			if($user = \Model\Auth_User::find_by_id($user))
			{
				// do we have this hash for this user, and hasn't it expired yet (we allow for 24 hours response)?
				if(isset($user->lostpassword_hash) and $user->lostpassword_hash == $hash and time()
						- $user->lostpassword_created < 86400)
				{
					// invalidate the hash
					\Auth::update_user(
							array(
						'lostpassword_hash' => null,
						'lostpassword_created' => null
							), $user->username
					);

					// log the user in and go to the profile to change the password
					if(\Auth::instance()->force_login($user->id))
					{
						// \Messages::info(__('login.password-recovery-accepted'));
						\Response::redirect('profile');
					}
				}
			}

			// something wrong with the hash
			// \Messages::error(__('login.recovery-hash-invalid'));
			\Response::redirect_back();
		}

		// no form posted, and no hash present. no clue what we do here
		else
		{
			\Response::redirect_back();
		}
	}

}
