<?php

/**
 * Description of main
 *
 * @author Lenak
 */

class Controller_Main extends \Fuel\Core\Controller_Template
{
	public $template = 'template';
	
	public function before()
	{
		parent::before();
		
		Lang::load(Config::get('language'));
		
		
		if(Auth::check())
		{
			
			$this->template->authCheck = true;
			$this->template->authUsername = Auth::get_screen_name();
		}
		else
			$this->template->authCheck = false;
		
		$this->template->css = [];
		$this->template->js = [];
		$this->template->cssFiles = [];
		$this->template->jsFiles = [];
	}
	
	protected function addCss($value)
	{
        array_push($this->template->cssFiles, $value);
	}
	
	protected function addJs($value)
	{
        array_push($this->template->jsFiles, $value);
	}
	
	protected function addInlineJs($value)
	{
        array_push($this->template->js, $value);
	}
	
	protected function addInlineCss($value)
	{
        array_push($this->template->css, $value);
	}
	
	protected function render($value)
	{
        $this->template->content = View::forge(Config::get('language').'/'.$value);
	}
	
	public function after($response)
	{
		$response = parent::after($response);
		return $response;
	}
}
