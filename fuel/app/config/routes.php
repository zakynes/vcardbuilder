<?php
return array(
	'_root_'  => 'Accueil/index',  // The default route
	//'_404_'   => 'Accueil/404',    // The main 404 route
	
	//Auth routes
	__('routes.auth.login') => 'User/login',
	__('routes.auth.logout') => 'User/logout',
	__('routes.auth.register') => 'User/register',
	__('routes.auth.lospassword') => 'User/lostpassword',
	
	//VCard routes
	'VCard/(:user)/(:id)' => 'VCard/view/$1/$2',
	'VCard/'.__('routes.vcard.infos').'/(:user)/(:id)' => 'VCard/infos/$1/$2',
	'VCard/'.__('routes.vcard.new') => 'VCard/new',
	'VCard/'.__('routes.vcard.save').'/infos' => 'VCard/saveInfos',
	'VCard/'.__('routes.vcard.save').'/design' => 'VCard/saveDesign',
	'VCard/'.__('routes.vcard.edit').'/infos/(:id)' => 'VCard/editInfos/$1',
	'VCard/'.__('routes.vcard.edit').'/design/(:id)' => 'VCard/editDesign/$1',
);